using {managed, sap} from '@sap/cds/common';
namespace rs.kitchenservice;

entity ServiceTime : managed {
    key Service_ID : Integer;
    time : localized String(100);
    names : localized String(500);
}

entity User : managed {
    key User_ID : Integer;
    email : localized String(100);
    password : localized String(100);
    admin : Boolean;
}