const cds = require('@sap/cds')
const { User } = cds.entities;
const { ServiceTime } = cds.entities;


module.exports = cds.service.impl((srv) => {
    srv.on("CREATE", getList);
});

getList = async (req)  =>  {

    const reqObject = req.data;
    const db = cds.transaction(req);
    
    const authentQuery = SELECT.from(User)
        .where("email"=reqObject.adminEmail)
        .where("password"=reqObject.adminPassword);

    const getListQuery = SELECT.form(ServiceTime)
        .columns("SERVICE_ID","time","names");
    
    if (db.run(authentQuery).length<1){
        req.error(409,"Du bist nicht berechtigt diese Liste einzusehen");
    }

    return db.run(getListQuery);
}
