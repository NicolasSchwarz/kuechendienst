const cds = require('@sap/cds')
const { User } = cds.entities


module.exports = cds.service.impl((srv) => {
    srv.on("CREATE", changeUser);
});

changeUser = async (req)  =>  {

    const reqObject = req.data;
    const db = cds.transaction(req);
    
    const authentQuery = SELECT.from(User)
        .where("email"=reqObject.adminEmail)
        .where("password"=reqObject.adminPassword)
        .where("admin"=true);

    const changeUserQuery = UPDATE(User)
        .columns("admin")
        .values(true)
        .where("User_ID"=reqObject.id);
    
    if (db.run(authentQuery).length<1){
        req.error(409,"Du bist nicht berechtigt einen Nutzer zum Admin zu machen");
    }

    return db.run(changeUserQuery);
}
