using { rs.kitchenservice as model} from '../db/schema';

service CreateUser {
entity User as SELECT from model.User {*} excluding {admin};
}