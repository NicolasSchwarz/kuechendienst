const cds = require('@sap/cds')
const { User } = cds.entities;
const { ServiceTime } = cds.entities;

module.exports = cds.service.impl((srv) => {
    srv.on("CREATE", setList);
});

setList = async (req)  =>  {

    const reqObject = req.data;
    const db = cds.transaction(req);
    
    const authentQuery = SELECT.from(User)
        .where("email"=reqObject.adminEmail)
        .where("password"=reqObject.adminPassword)
        .where("admin"=true);
    
    if (db.run(authentQuery).length<1){
        req.error(409,"Du bist nicht berechtigt einen Nutzer zum Admin zu machen");
    }

    for(let i=0;i<reqObject.list.length;i++){
        
        let changeUserQuery = INSERT.into(ServiceTime)
            .columns("time","names")
            .values(reqObject.list[i].time,reqObject.list[i].names);
        
            await db.run(changeUserQuery);
    }   
}
