const cds = require('@sap/cds')
const { User } = cds.entities


module.exports = cds.service.impl((srv) => {
    srv.on("CREATE", createUser);
});



createUser = async (req)  =>  {

    const reqObject = req.data;
    const db = cds.transaction(req);
    const existCheckQuery = SELECT.from(User).where("email"=reqObject.email);
    const createUserQuery = INSERT.into(User)
                                    .columns("email", "password","admin")
                                    .values(user.email, password, false);
    
    const checkPassword = passwordValidator(reqObject.password);
    const checkEmail = emailValidator(reqObject.email);

    if(checkEmail.code != 200){
        req.error(checkEmail.code, checkEmail.msg);
    }

    if(checkPassword.code != 200){
        req.error(checkPassword.code, checkPassword.msg);
    }

    if((await db.run(existCheckQuery)).length>0){
        req.error(409,"Nutzer mit dieser Email-Adresse existiert bereits.");
    }

    return db.run(createUserQuery);

}

passwordValidator = (passwd) => {

    const numbers = /^\d+$/;
    const specialChars = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/;
    const lowerCaseChars = /[a-z]/;
    const upperCaseChars = /[A-Z]/;

    let response = {"code":200,"msg":""};

    if(passwd.length<7 || passwd.length>15){
        response.code = 409;
        response.msg = "Das Passwort muss mindestens 7 und darf maximal 15 Zeichen haben";
    }

    if(passwd.search(numbers) <1){
        response.code = 409;
        response.msg = "Das Passwort muss Zahlen enthalten";
    }

    if (passwd.search(lowerCaseChars) < 1 || passwd.search(upperCaseChars) < 1) {
        response.code = 409;
        response.msg = "Das Passwort muss Groß- und Kleinbuchstaben enthalten";
    }

    if(passwd.search(specialChars) <1){
        response.code = 409;
        response.msg = "Das Passwort Sonderzeichen enthalten";
    }

    return response;
}

emailValidator = (email) => {

    let response = {"code":200,"msg":""};

    if (!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)){
        response.code = 409;
        response.msg = "Die Email-Adresse ist nicht valide.";
    }  
    
    return response;
}